package pk.labs.LabC.animal3.internal;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import pk.labs.LabC.contracts.Animal;

public class SlonAfrykanski implements Animal {

    static SlonAfrykanski instance;

    private final String species;
    private final String name;
    private String status;

    public SlonAfrykanski() {
        this.species = "Slon";
        this.name = "Slon Afrykanski";
        this.status = "Bezczynny";
    }

    public static SlonAfrykanski get() {
        return instance;
    }

    @Override
    public String getSpecies() {
        return this.species;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String getStatus() {
        return this.status;
    }

    @Override
    public void setStatus(String status) {
        this.status = status;
    }

    private final PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);

    @Override
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.addPropertyChangeListener(listener);
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.removePropertyChangeListener(listener);
    }
}