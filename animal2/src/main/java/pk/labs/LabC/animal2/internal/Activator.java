package pk.labs.LabC.animal2.internal;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;
import pk.labs.LabC.contracts.Animal;
import pk.labs.LabC.logger.Logger;

public class Activator implements BundleActivator {

    static ServiceRegistration serv;
    
    @Override
    public void start(BundleContext context) throws Exception {
        DelfinBialonosy.instance = new DelfinBialonosy();
        Logger.get().log(this, "Delfin - ON");
        Activator.serv = context.registerService(Animal.class.getName(), DelfinBialonosy.instance, null);
    }

    @Override
    public void stop(BundleContext context) throws Exception {
        Logger.get().log(this, "Delfin - OFF");
        DelfinBialonosy.instance = null;
        serv.unregister();
    }
}
