package pk.labs.LabC.animal2.internal;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import pk.labs.LabC.contracts.Animal;

public class DelfinBialonosy implements Animal {

    static DelfinBialonosy instance;

    private final String species;
    private final String name;
    private String status;

    public DelfinBialonosy() {
        this.species = "Delfin";
        this.name = "Delfin Bialonosy";
        this.status = "Bezczynny";
    }

    public static DelfinBialonosy get() {
        return instance;
    }

    @Override
    public String getSpecies() {
        return this.species;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String getStatus() {
        return this.status;
    }

    @Override
    public void setStatus(String status) {
        this.status = status;
    }

    private final PropertyChangeSupport popertyChangeSupport = new PropertyChangeSupport(this);

    @Override
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        popertyChangeSupport.addPropertyChangeListener(listener);
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        popertyChangeSupport.removePropertyChangeListener(listener);
    }
}
