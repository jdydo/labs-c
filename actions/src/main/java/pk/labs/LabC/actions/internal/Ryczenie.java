package pk.labs.LabC.actions.internal;

import pk.labs.LabC.contracts.Animal;
import pk.labs.LabC.contracts.AnimalAction;

public class Ryczenie implements AnimalAction {

    public static Ryczenie instance;
    private final String name;

    public Ryczenie() {
        name = "Ryczenie";
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean execute(Animal animal) {
        animal.setStatus("Ryczy");
        return true;
    }

}
