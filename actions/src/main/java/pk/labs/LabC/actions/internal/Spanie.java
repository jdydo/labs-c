package pk.labs.LabC.actions.internal;

import pk.labs.LabC.contracts.Animal;
import pk.labs.LabC.contracts.AnimalAction;

public class Spanie implements AnimalAction {

    public static Spanie instance;
    private final String name;

    public Spanie() {
        name = "Spanie";
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean execute(Animal animal) {
        animal.setStatus("Spi");
        return true;
    }
}

