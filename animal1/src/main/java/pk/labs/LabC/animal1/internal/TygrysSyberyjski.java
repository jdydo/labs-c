package pk.labs.LabC.animal1.internal;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import pk.labs.LabC.contracts.Animal;

public class TygrysSyberyjski implements Animal {

    static TygrysSyberyjski instance;

    private final String species;
    private final String name;
    private String status;

    public TygrysSyberyjski() {
        this.species = "Tygrys";
        this.name = "Tygrys syberyjski";
        this.status = "Bezczynny";
    }

    public static TygrysSyberyjski get() {
        return instance;
    }

    @Override
    public String getSpecies() {
        return this.species;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String getStatus() {
        return this.status;
    }

    @Override
    public void setStatus(String status) {
        this.status = status;
    }

    private final PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);

    @Override
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.addPropertyChangeListener(listener);
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.removePropertyChangeListener(listener);
    }
}
