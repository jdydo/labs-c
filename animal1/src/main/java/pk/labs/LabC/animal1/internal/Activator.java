package pk.labs.LabC.animal1.internal;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;
import pk.labs.LabC.contracts.Animal;
import pk.labs.LabC.logger.Logger;

public class Activator implements BundleActivator {

    static ServiceRegistration serv;
    
    @Override
    public void start(BundleContext context) throws Exception {
        TygrysSyberyjski.instance = new TygrysSyberyjski();
        Logger.get().log(this, "Tygrys - ON");
        Activator.serv = context.registerService(Animal.class.getName(), TygrysSyberyjski.instance, null);
    }

    @Override
    public void stop(BundleContext context) throws Exception {
        Logger.get().log(this, "Tygrys - OFF");
        TygrysSyberyjski.instance = null;
        serv.unregister();
    }
}
