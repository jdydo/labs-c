package pk.labs.LabC.zoo;

import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.util.tracker.ServiceTrackerCustomizer;
import pk.labs.LabC.contracts.Animal;

public class Customizer implements ServiceTrackerCustomizer {

    private final BundleContext context;

    public Customizer(BundleContext context) {
        this.context = context;
    }

    @Override
    public Object addingService(ServiceReference sr) {
        Animal animal = (Animal) context.getService(sr);
        Zoo.get().addAnimal(animal);
        System.out.println("Dodaje " + animal.getName());
        return animal;
    }

    @Override
    public void modifiedService(ServiceReference sr, Object o) {
    }

    @Override
    public void removedService(ServiceReference sr, Object o) {
        Animal animal = (Animal) o;
        Zoo.get().removeAnimal(animal);
        System.out.println("Usuwam " + animal.getName());
    }
}
